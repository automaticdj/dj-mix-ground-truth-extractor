# Miroslav Kovalenko
# Change the filenames to eliminate not accepted characters
import os
import argparse
import re

def main():
    # Parse arguments
    parser = argparse.ArgumentParser(description='Rename files in the directory')

    parser.add_argument(
        'dir',
        metavar='<dir>',
        action='store',
        nargs=1,
        help='Path to dir which files needs to be renamed')

    args = parser.parse_args()


    # Do renaming
    for filename in os.listdir(args.dir[0]):
        path_to_file = os.path.join(args.dir[0],filename)
        new_filename = re.sub('[^a-zA-Z0-9 \-\.]', '', filename)
        new_path_to_file = os.path.join(args.dir[0],new_filename)
        if path_to_file.endswith('.wav'): # no directories and other files
            os.rename(path_to_file,new_path_to_file)

if __name__ == '__main__':
    main()