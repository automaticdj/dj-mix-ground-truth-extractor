import numpy as np
import pandas as pd
from scipy import stats
import logging
import itertools


class MatchInfo:
    def __init__(self, path, ident, slope, offset, s):
        self.path = path
        self.ident = ident
        self.bounds = (s[0], s[-1])
        self.slope = slope
        self.offset = offset

    def __str__(self):
        return f"{self.path} - {self.ident} - {self.offset}"


def parse_data(filename):
    return pd.read_csv(filename, sep=';')


def calculate_segments(initial_dataframe, identifier):
    # skip first element, panako makes this unreliable
    initial_dataframe = initial_dataframe[1:]

    flatten = lambda l: [item for sublist in l for item in sublist]

    df = initial_dataframe[initial_dataframe[' Match Identifier'] == identifier][[' Match start (s)']]
    df.columns = ['match_start']

    if len(df) < 20:
        return None

    logging.info('segment finder for '+("src_tracks/" + initial_dataframe[initial_dataframe[' Match Identifier'] == identifier].iloc[0]['Match description']))

    # Extract the general slope
    a_bounds = (0.98, 1.02)
    n_bins = 10000
    bins = np.linspace(*a_bounds, n_bins)
    hist = pd.Series(np.zeros(n_bins), index=bins)
    series = df['match_start']
    for i in series.index:
        for j in range(2, 300):
            if i + j not in series.index:
                continue
            a_ij = (series[i + j] - series[i])/float(j)
            if a_bounds[0] < a_ij < a_bounds[1]:
                hist[bins[np.digitize(a_ij, bins)]] += 1

    hist[np.abs(hist.index - 1) < 0.000005] = 0
    a = hist.idxmax()
    if abs(a - 1) < 0.005:
        a = 1
    a_old = a
    a = 1.
    logging.warn('stretch value is changed from ' + str(a_old) + ' to '+str(a))

    # Extract offset
    med = (df['match_start'] - a * df.index).median()
    estimate = stats.gaussian_kde(df['match_start'] - a * df.index)
    rnge = np.linspace(med - 750, med + 750, 50000)
    ests = estimate(rnge)
    amx = ests.argmax()
    mx = ests.max()

    al = np.argmax(ests[:amx] >= mx / 2)
    ar = 25000 + np.argmax(ests[amx:] <= mx / 2)
    bounds = (rnge[np.clip(al, 0, 50000 - 1)], rnge[np.clip(ar, 0, 50000 - 1)])

    points = (df['match_start'] - a * df.index)
    lb, rb = bounds
    points_in_bounds = points[(points > lb) & (points < rb)]

    segs = [[]]
    for i, x in enumerate(points_in_bounds.index):
        if i + 1 >= len(points_in_bounds.index):
            break

        idx = points_in_bounds.index
        di = idx[i + 1] - idx[i]

        if di > 10:
            segs.append([])

        segs[-1].append(idx[i])

    segs = sorted(segs, key=len)
    segs = [s for s in segs if len(s) > 20]
    segs = [s for s in segs if s[-1] - s[0] >= 5]

    # TODO: Remove this hardcoded stuff
    path = "src_tracks/" + initial_dataframe[initial_dataframe[' Match Identifier'] ==
                                             identifier].iloc[0]['Match description']

    offset = (amx/50000)*1500 - 750 + med
    offset_old = offset

    # Option 1
    # offset = (df['match_start'] - a * df.index).value_counts().idxmax()

    # Option 2 & 3
    most_counts_value = (df['match_start'] - a * df.index).value_counts().idxmax()
    df_match = df['match_start'] - a * df.index
    # middle_matches = df_match.sort_values()[int(len(df_match) / 2) - min(6, int(len(df_match)/2)):int(len(df_match) / 2) + min(6, int(len(df_match)/2))]
    # counts = df_match.value_counts().tolist()
    # values = df_match.value_counts().keys().tolist()
    # most_counts_matches = [[value]*int(count*1/(abs(value-most_counts_value)+1)) if (count > 0) else [] for (value,count) in zip(values,counts)]
    # most_counts_matches = list(itertools.chain.from_iterable(most_counts_matches))

    disrupt_factor = 6
    stabilise_factor = 4
    most_counts_matches = df_match.sort_values()[int(len(df_match) / 2) - min(disrupt_factor, int(len(df_match) / 2)):int(len(df_match) / 2) + min(
        disrupt_factor, int(len(df_match) / 2))]
    most_counts_matches = most_counts_matches.tolist()
    most_counts_matches += [most_counts_value]*(int(len(most_counts_matches) / stabilise_factor))
    if len(set(most_counts_matches)) != 1:
        estimate = stats.gaussian_kde(most_counts_matches)
        rnge = np.linspace(most_counts_value - .5, most_counts_value + .5, 100)
        ests = estimate(rnge)
        amx = ests.argmax()
        offset = (amx / 100) * 1 - .5 + most_counts_value
    else:
        offset = most_counts_value

    logging.warn('offset value is changed from ' + str(offset_old) + ' to '+str(offset))
                 # '. Possible values were:\n==========\n'+str((df['match_start'] - a * df.index).value_counts()) +
                 # '\n==========')

    if len(segs) > 0:
        # a_old = a
        # offset_old = offset
        # file_name_t1 = 'Fox Stevenson - Bruises www.my-free-mp3.net'
        # file_name_t2 = 'Muzzy  Flite - Elevate feat. Miss Trouble www.my-free-mp3.net'
        # if path == f'src_tracks/{file_name_t1}.wav':
        #     a = 1.
        #     offset = -0.136
        # elif path == f'src_tracks/{file_name_t2}.wav':
        #     a = 0.994
        #     offset = -88.338
        # else:
        #     logging.warn('skipping track')
        #     return None
        # if a_old != a:
        #     logging.warn('stretch value is changed 2nd time from ' + str(a_old) + ' to ' + str(a))
        # if offset_old != offset:
        #     logging.warn('offset value is changed from ' + str(offset_old) + ' to ' + str(offset))
        return MatchInfo(path, identifier, a, offset, flatten(sorted(segs, key=lambda x: x[0])))
    else:
        return None


class SegmentFinder:
    provides = ['segments']
    needs = ['fingerprints']

    def process(self, state):
        df = state['fingerprints']

        identifiers = df[' Match Identifier'].unique()
        matches = [calculate_segments(df, ident) for ident in identifiers]
        matches = [m for m in matches if m]

        return {**state, 'segments': matches}
