# DJ Mix Ground Truth Extractor

This software allows for reverse engineering actions performed by a DJ in a mix.

**Note:** this is a fork, used in my dissertation , see project repo [here](https://bitbucket.org/automaticdj/dj). 

Original repository is located [here](https://github.com/werthen/dj-mix-ground-truth-extractor).

## Getting Started

### Prerequisites

[Panako](http://panako.be/) is used for fingerprinting and must be installed beforehand.

### Installing

```
pip install -r requirements.txt
```

### Usage

```
$ python main.py -h
usage: main.py [-h] <mix_path> <source_track_directory_path> <output_file>

Analyse a DJ mix and reverse engineer it

positional arguments:
  <mix_path>            Path to a mix which needs to be analysed
  <source_track_directory_path>
                        Path to all source tracks
  <output_file>         Path to the output JSON file

optional arguments:
  -h, --help            show this help message and exit
```

## Copyright information


Copyright (c) 2018 Lorin Werthen-Brabants Initial work. This software is complementary to my dissertation. The evaluation dataset is available on the following link: <https://1drv.ms/f/s!Al5-09HWyOJ3wlvkkd0ZGte119ov>

Copyright (c) 2019 Miroslav Kovalenko <MiroslavKovalenko@icloud.com> Adpated version, used in my dissertation "Creating coherent cross-fades in an automatic DJ software tool using a machine learning approach", to primarily extract offsets and cross-fade profiles. See [project repo](https://bitbucket.org/automaticdj/dj).

See also the list of [contributors](https://bitbucket.org/automaticdj/dj-mix-ground-truth-extractor/commits/all) who participated in this project.


## License

This project is licensed under the MIT License - see the [LICENSE](LICENSE) file for details
